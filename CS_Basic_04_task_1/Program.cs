﻿using System;

namespace CS_Basic_04_task_1
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Square A = new Square();
            A.SquareSide = 3;

            Console.Write(A.SquarePerimeter());
            Console.Write(A.SquareArea());
        }

        public class Square
        {
            public double SquareSide;

            public double SquarePerimeter()
            {
                return SquareSide * 4;
            }

            public double SquareArea()
            {
                return SquareSide * SquareSide;
            }

        }

    }
}
